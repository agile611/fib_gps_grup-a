package test.integration.org.testinfected.petstore.jdbc;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.db.JDBCTransactor;
import org.testinfected.petstore.db.OrdersDatabase;
import org.testinfected.petstore.order.Order;
import org.testinfected.petstore.order.OrderNumber;
import org.testinfected.petstore.product.Product;
import org.testinfected.petstore.transaction.Transactor;
import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.fails;
import static test.support.org.testinfected.petstore.builders.OrderBuilder.anOrder;
import static test.support.org.testinfected.petstore.builders.CartBuilder.aCart;
import static test.support.org.testinfected.petstore.builders.ItemBuilder.anItem;
import static test.support.org.testinfected.petstore.builders.ProductBuilder.aProduct;
import test.support.org.testinfected.petstore.jdbc.Database;

import java.sql.Connection;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static org.hamcrest.Matchers.equalTo;
import static test.support.org.testinfected.petstore.builders.OrderBuilder.anOrder;

public class OrdersDatabaseTest {
	Database database;
    Connection connection;
    Transactor transactor;
    
    OrdersDatabase ordersDatabase;
    
    @Before public void
    setUp() throws SQLException {
    	database = Database.test();
    	connection = database.connect();
    	transactor = new JDBCTransactor(connection);
        ordersDatabase = new OrdersDatabase(connection);
    }
    
    @After public void
    tearDown() throws SQLException {
        connection.close();
    }
    
    @Test public void
    trobarComandesPerNumero() {
    	Order comanda = anOrder().withNumber("1").build();
    	ordersDatabase.record(comanda);
    	assertThat("Trobar per numero", validationOf(ordersDatabase.find(new OrderNumber("1"))), succeeds());
    }
    
    @Test public void
    elementsMateixTipus() {
    	Product prod = aProduct().build();
    	assertThat("No hi ha errors amb elements del mateix tipus", validationOf(anOrder().from(aCart().containing(anItem().of(prod),anItem().of(prod)))),succeeds());
    }
}
