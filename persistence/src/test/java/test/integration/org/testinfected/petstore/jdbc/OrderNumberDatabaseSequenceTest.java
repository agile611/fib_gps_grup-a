package test.integration.org.testinfected.petstore.jdbc;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.db.JDBCTransactor;
import org.testinfected.petstore.db.OrdersDatabase;
import org.testinfected.petstore.order.Order;
import org.testinfected.petstore.order.OrderNumber;
import org.testinfected.petstore.transaction.Transactor;

import test.support.org.testinfected.petstore.builders.OrderBuilder;
import test.support.org.testinfected.petstore.jdbc.Database;

import java.sql.Connection;
import java.sql.SQLException;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static test.support.org.testinfected.petstore.builders.OrderBuilder.anOrder;


public class OrderNumberDatabaseSequenceTest {

	Database database;
    Connection connection;
    Transactor transactor;

    OrdersDatabase ordersDatabase;

    
    @Before public void
    setUp() throws SQLException {
    	database = Database.test();
    	connection = database.connect();
    	transactor = new JDBCTransactor(connection);
        ordersDatabase = new OrdersDatabase(connection);
    }
    
    @After public void
    tearDown() throws SQLException {
        connection.close();
    }
    
    @Test public void
    incrementsNumComanda() {
    	OrderBuilder ob1 = anOrder().withNumber("1");
    	Order o1 = ob1.build();
    	OrderBuilder ob2 = anOrder().withNumber("2");
    	Order o2 = ob2.build();
    	ordersDatabase.record(o1);
    	ordersDatabase.record(o2);
    	
    	Order found = ordersDatabase.find(new OrderNumber("1"));
    	assertThat("validation of incremental ids", found, hasOrderNumber("1"));
    	
    }
    
    private Matcher<Order> hasOrderNumber(final String number) {
        return new FeatureMatcher<Order, String>(equalTo(number), "has order number", "order number") {
            protected String featureValueOf(Order actual) {
                return actual.getNumber();
            }
        };
    } 
}