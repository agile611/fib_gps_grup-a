package test.integration.org.testinfected.petstore.jdbc;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.db.JDBCTransactor;
import org.testinfected.petstore.db.OrdersDatabase;
import org.testinfected.petstore.db.ProductsDatabase;
import org.testinfected.petstore.order.Order;
import org.testinfected.petstore.order.OrderNumber;
import org.testinfected.petstore.product.DuplicateProductException;
import org.testinfected.petstore.product.Product;
import org.testinfected.petstore.transaction.Transactor;
import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.fails;
import static test.support.org.testinfected.petstore.builders.OrderBuilder.anOrder;
import static test.support.org.testinfected.petstore.builders.CartBuilder.aCart;
import static test.support.org.testinfected.petstore.builders.ItemBuilder.anItem;
import static test.support.org.testinfected.petstore.builders.ProductBuilder.aProduct;

import test.support.org.testinfected.petstore.builders.ProductBuilder;
import test.support.org.testinfected.petstore.jdbc.Database;

import java.sql.Connection;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static org.hamcrest.Matchers.equalTo;
import static test.support.org.testinfected.petstore.builders.OrderBuilder.anOrder;

public class ProductsDatabaseTest {
	Database database;
    Connection connection;
    Transactor transactor;
    
    ProductsDatabase productsDatabase;
    
    @Before public void
    setUp() throws SQLException {
    	database = Database.test();
    	connection = database.connect();
    	transactor = new JDBCTransactor(connection);
    	
    	productsDatabase = new ProductsDatabase(connection);
    }
    
    @After public void
    tearDown() throws SQLException {
        connection.close();
    }
    
    @Test public void
    trobarProductesPerNumero() throws DuplicateProductException {
    	Product prod = aProduct().withNumber("1").build();
    	productsDatabase.add(prod);
    	assertThat("Trobar per numero", validationOf(productsDatabase.findByNumber("1")), succeeds());
    }
    
    @Test public void
    trobarProductesPerNom() throws DuplicateProductException {
    	Product prod = aProduct().named("nom").build();
    	productsDatabase.add(prod);
    	assertThat("Trobar per nom", productsDatabase.findByKeyword("nom").get(0).getName().equals("nom"));
    }
    
    @Test public void
    trobarProductesPerDescr() throws DuplicateProductException {
    	Product prod = aProduct().describedAs("descripcio").build();
    	productsDatabase.add(prod);
    	assertThat("Trobar per descripcio", productsDatabase.findByKeyword("descripcio").get(0).getDescription().equals("descripcio"));
    }
    
    @Test public void
    recuperarInfoProducte() throws DuplicateProductException{
    	Product prod = aProduct().withNumber("1").named("nom").describedAs("descripcio").build();
    	productsDatabase.add(prod);
    	prod = null;
    	
    	Product recuperacio =  productsDatabase.findByNumber("1");
    	assertThat("Es pot recuperar Info producte", recuperacio.getNumber().equals("1") && recuperacio.getName().equals("nom") && recuperacio.getDescription().equals("descripcio"));
    }
    
    @Test(expected = DuplicateProductException.class)
    public void identificadorUnic() throws DuplicateProductException{
    	Product prod1 = aProduct().build();
    	productsDatabase.add(prod1);
    	productsDatabase.add(prod1);
    }
    
    @Test(expected=Exception.class)
    public void producteNoExistent(){
    	assertThat("Producte no existent", validationOf(productsDatabase.findByNumber("1")),fails());
    }
}

