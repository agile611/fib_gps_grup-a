package test.unit.org.testinfected.petstore.product;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.builders.AddressBuilder.anAddress;
import static test.support.org.testinfected.petstore.builders.ProductBuilder.*;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.on;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.*;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.violates;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.withMessage;

import org.junit.Test;
import org.testinfected.petstore.product.Product;

import test.support.org.testinfected.petstore.builders.ProductBuilder;

public class ProductTest {

	
    @Test public void
    teFotoPerDefecte() {
    	Product prod = ProductBuilder.aProduct().build();
        assertThat("valida que un producte te la foto per defecte", prod.getPhotoFileName().equals(Product.MISSING_PHOTO));
    }
    
    @Test public void
    UnicNumeroReferencia() {
        assertThat("valida que el producte s identifia per numero de referencia", aProduct().build().getNumber()!=aProduct().build().getNumber());
    }

}
