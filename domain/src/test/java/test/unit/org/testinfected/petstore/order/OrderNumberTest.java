package test.unit.org.testinfected.petstore.order;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.builders.FakeOrderNumber.anOrderNumber;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;

public class OrderNumberTest {

	@Test public void
	uniqueNumber() {
		assertThat("validation of unique order number", anOrderNumber().getNumber() != anOrderNumber().getNumber());
	}
	
	@Test public void
	normalizeNumber() {
		assertThat("validation of normalize cart number", anOrderNumber().getNumber().length()==8);
	}
	
}