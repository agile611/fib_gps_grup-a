package test.unit.org.testinfected.petstore.product;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.builders.ItemBuilder.anItem;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;

public class ItemTest {
	
	@Test public void
    esIdentificatPelNumero() {
        assertThat("validacio de la referencia d'item per numero", anItem().build().getNumber()!=anItem().build().getNumber());
    }
}