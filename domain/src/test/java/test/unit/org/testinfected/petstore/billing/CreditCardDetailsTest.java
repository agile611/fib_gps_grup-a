package test.unit.org.testinfected.petstore.billing;

import org.junit.Test;
import org.testinfected.petstore.billing.Address;
import org.testinfected.petstore.billing.CreditCardDetails;
import org.testinfected.petstore.billing.CreditCardType;

import test.support.org.testinfected.petstore.builders.AddressBuilder;
import test.support.org.testinfected.petstore.builders.CreditCardBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.builders.AddressBuilder.anAddress;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.on;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.*;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.violates;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.withMessage;


public class CreditCardDetailsTest {
	@Test
	public void validacioNumeracio() {
		assertThat("validation of Numeracio", validationOf(CreditCardBuilder.aVisa().withNumber("4111111")), violates(on("cardNumber"), withMessage("incorrect")));
	}
	
	@Test
	public void validacioDataDeCaducitat() {
		assertThat("validation of Data de Caducitat", validationOf(CreditCardBuilder.aCreditCard().withExpiryDate(null)), violates(on("cardExpiryDate"), withMessage("missing")));
	}
	
	@Test
	public void validacioNomTitular() {
		assertThat("validation of Data de Caducitat", validationOf(CreditCardBuilder.aCreditCard().billedTo(AddressBuilder.anAddress().withFirstName(null))), violates(on("billingAddress"),withMessage("invalid")));
	}
	
	@Test
	public void validacioTotsCampsCorrectes() {
		assertThat("validation of Data de Caducitat", validationOf(CreditCardBuilder.aCreditCard()), succeeds());
	}
	

}
