package test.unit.org.testinfected.petstore.order;

import org.junit.Test;
import org.testinfected.petstore.billing.Address;
import org.testinfected.petstore.billing.CreditCardDetails;
import org.testinfected.petstore.billing.CreditCardType;
import org.testinfected.petstore.order.Cart;
import org.testinfected.petstore.product.Item;
import org.testinfected.petstore.product.Product;

import test.support.org.testinfected.petstore.builders.AddressBuilder;
import test.support.org.testinfected.petstore.builders.CartBuilder;
import test.support.org.testinfected.petstore.builders.CreditCardBuilder;
import test.support.org.testinfected.petstore.builders.ItemBuilder;
import test.support.org.testinfected.petstore.builders.ProductBuilder;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.builders.AddressBuilder.anAddress;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.on;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.*;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.violates;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.withMessage;

import java.math.BigDecimal;

public class CartItemTest {
	
	@Test
	public void validationOfUnElement() {
		Cart cistella = CartBuilder.aCart().containing(ItemBuilder.anItem()).build();
		assertThat("validation of 1 element", cistella.getItems().size()==1);
	}
	
	@Test
	public void validationOfSumaCistella() {
		Cart cistella = CartBuilder.aCart().containing(ItemBuilder.anItem().priced("12"), ItemBuilder.anItem().priced("3")).build();
		BigDecimal sumaTotal = new BigDecimal("15");
		assertThat("validation of Suma Cistella", cistella.getGrandTotal().equals(sumaTotal));
	}
	
	@Test
	public void validationOfDetall() {
		Item item = ItemBuilder.anItem().describedAs("d").build();
		Cart cistella = CartBuilder.aCart().containing(item).build();
		assertThat("validation of Detall", cistella.getItems().get(0).getItemDescription().equals("d"));
	}
	
	@Test
	public void validationOfActualitzacioPreu() {
		Item item1 = ItemBuilder.anItem().describedAs("d").priced("10").build();
		Item item2 = ItemBuilder.anItem().describedAs("d").priced("5").build();
		Cart cistella = CartBuilder.aCart().containing(item1,item2).build();
		
		BigDecimal suma = new BigDecimal("15");
		assertThat("validation of ActualtizacioPreu", cistella.getGrandTotal().equals(suma));
	}

}
