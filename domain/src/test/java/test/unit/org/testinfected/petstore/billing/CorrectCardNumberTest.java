package test.unit.org.testinfected.petstore.billing;

import org.junit.Test;
import org.testinfected.petstore.billing.CreditCardType;

import static org.hamcrest.MatcherAssert.assertThat;
import static test.support.org.testinfected.petstore.builders.CreditCardBuilder.aVisa;
import static test.support.org.testinfected.petstore.builders.CreditCardBuilder.aCreditCard;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.succeeds;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.fails;
import static test.support.org.testinfected.petstore.matchers.ValidationMatchers.validationOf;

public class CorrectCardNumberTest {
	
	@Test public void
	visaBegins4() {
		assertThat("validation of visa", validationOf(aVisa().withNumber("4111222233334444")), succeeds());
	}
	
	@Test public void
	mastercardBegins5() {
		assertThat("validation of mastercard", validationOf(aCreditCard().ofType(CreditCardType.mastercard).withNumber("5111222233334444")), succeeds());
	}
	
	@Test public void
	americanBegins3() {
		assertThat("validation of american express", validationOf(aCreditCard().ofType(CreditCardType.amex).withNumber("347122223333444")), succeeds());
	}
	
	@Test public void
	notVisa() {
		assertThat("the credit card is not a visa", validationOf(aCreditCard().ofType(CreditCardType.amex)), fails());
	}
	
}

